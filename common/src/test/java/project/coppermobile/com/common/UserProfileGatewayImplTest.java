package project.coppermobile.com.common;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import project.coppermobile.com.common.cache.UserProfileData;
import project.coppermobile.com.common.gateway.UserProfileGatewayImpl;
import project.coppermobile.com.common.response.UserProfile;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserProfileGatewayImplTest {

    private UserProfile userProfile;
    private UserProfileData userProfileData;

    @Before
    public void text_UserprofileData() {
        Context context = mock(Context.class);
        when(context.getApplicationContext()).thenReturn(mock(Context.class));

        UserProfileGatewayImpl userProfileGateway = new UserProfileGatewayImpl();
        userProfileData = UserProfileData.getInstance();

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            URL resource = classLoader.getResource("UserProfileInfo.json");
            File file = new File(resource.getPath());

            InputStream inputStream = new FileInputStream(file);
            String jsonResponse = inputStreamToString(inputStream);
            userProfile = userProfileGateway.parseJsonConfiguration(jsonResponse);
        } catch (FileNotFoundException e) {
            e.getMessage();
        }
        userProfileData.setUserProfileData(userProfile);
    }

    @Test
    public void UserProfileDataTest(){
        String firstName = "Sri Harsha";
        String lastName = "Kunapuli";
        String email = "harsha.s@coppermovile.com";
        String password = "coppermobile";
        if (userProfileData != null && userProfileData.getUserProfileData() != null) {
            String fnResult = userProfileData.getUserProfileData().getUserProfile().getFirstName();
            String lnResult = userProfileData.getUserProfileData().getUserProfile().getLastName();
            String emailResult = userProfileData.getUserProfileData().getUserProfile().getEmail();
            String passwordResult = userProfileData.getUserProfileData().getUserProfile().getPassword();
            assertThat(firstName, is(fnResult));
            assertThat(lastName, is(lnResult));
            assertThat(email, is(emailResult));
            assertThat(password, is(passwordResult));
        }
    }

    private String inputStreamToString(InputStream is) {
        BufferedReader rd = new BufferedReader(new InputStreamReader(is), 4096);
        String line;
        StringBuilder sb =  new StringBuilder();
        try {
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
