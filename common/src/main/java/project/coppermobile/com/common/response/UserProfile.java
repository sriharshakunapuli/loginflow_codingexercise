
package project.coppermobile.com.common.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile {

    @SerializedName("userProfile")
    @Expose
    private UserData userProfile;

    public UserData getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserData userProfile) {
        this.userProfile = userProfile;
    }

}
