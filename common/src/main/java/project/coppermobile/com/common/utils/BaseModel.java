package project.coppermobile.com.common.utils;

import android.os.Bundle;
import android.util.SparseArray;

public class BaseModel {

    private SparseArray<BaseModel> mBaseModelSparseArray;

    public BaseModel(BaseModel... baseModels){
        mBaseModelSparseArray = new SparseArray<>(baseModels.length);
        for(int index=0; index<baseModels.length; index++){
            mBaseModelSparseArray.put(index, baseModels[index]);
        }
    }

    public void onCreate(Bundle savedInstanceState, Bundle extras) {
        for(int index=0; index<mBaseModelSparseArray.size(); index++){
            mBaseModelSparseArray.get(index).onCreate(savedInstanceState, extras);
        }
    }

    public void onStart() {
        for(int index=0; index<mBaseModelSparseArray.size(); index++){
            mBaseModelSparseArray.get(index).onStart();
        }
    }

    public void onStop() {
        for(int index=0; index<mBaseModelSparseArray.size(); index++){
            mBaseModelSparseArray.get(index).onStop();
        }
    }

    public void onDestroy() {
        for(int index=0; index<mBaseModelSparseArray.size(); index++){
            mBaseModelSparseArray.get(index).onDestroy();
        }
    }
}
