package project.coppermobile.com.common.cache;

import project.coppermobile.com.common.response.UserProfile;

public class UserProfileData {

    private UserProfile userProfile;
    private static UserProfileData mInstance;

    private UserProfileData() {
    }

    public static UserProfileData getInstance() {
        if (mInstance == null) {
            mInstance = new UserProfileData();
        }

        return mInstance;
    }

    public void setUserProfileData(UserProfile userProfileResponse){
        userProfile = userProfileResponse;
    }

    public UserProfile getUserProfileData() {
        /*if (userProfile == null) {
            UserProfileGateway gateway = UserProfileGatewayProvider.getConfigGateway();
            setUserProfileData(gateway.readSavedProfile());
        }*/

        return userProfile;
    }
}
