package project.coppermobile.com.common.viewmodels;

import android.databinding.ObservableField;

import project.coppermobile.com.common.cache.UserProfileData;
import project.coppermobile.com.common.models.InformationModel;
import project.coppermobile.com.common.utils.BaseViewModel;

public class InformationViewModel extends BaseViewModel {

    private InformationModel mModel;
    private UserProfileData userProfileData;

    private ObservableField<String> givenName;
    private ObservableField<String> lastName;
    private ObservableField<String> emailIdAttrib;
    private ObservableField<String> passwordAttrib;

    public InformationViewModel(InformationModel informationModel) {
        super();
        mModel = informationModel;
        userProfileData = UserProfileData.getInstance();

        givenName = new ObservableField<>("");
        lastName = new ObservableField<>("");
        emailIdAttrib = new ObservableField<>("");
        passwordAttrib = new ObservableField<>("");
    }

    public void startLoading() {}

    public void displayInfo() {
        if (userProfileData != null && userProfileData.getUserProfileData() != null
                && userProfileData.getUserProfileData().getUserProfile() != null) {
            givenName.set(userProfileData.getUserProfileData().getUserProfile().getFirstName());
            lastName.set(userProfileData.getUserProfileData().getUserProfile().getLastName());
            emailIdAttrib.set(userProfileData.getUserProfileData().getUserProfile().getEmail());
            passwordAttrib.set(userProfileData.getUserProfileData().getUserProfile().getPassword());
        }
    }

    public ObservableField<String> getGivenName() {
        return givenName;
    }

    public ObservableField<String> getLastName() {
        return lastName;
    }

    public ObservableField<String> getEmailIdAttrib() {
        return emailIdAttrib;
    }

    public ObservableField<String> getPasswordAttrib() {
        return passwordAttrib;
    }

    public void clearSavedInfo() {
        mModel.clearSavedUserProfileFile();
    }
}
