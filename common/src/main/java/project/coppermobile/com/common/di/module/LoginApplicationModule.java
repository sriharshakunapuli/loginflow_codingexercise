package project.coppermobile.com.common.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginApplicationModule {

    private Context mContext;

    public LoginApplicationModule(Context context) {
        mContext = context.getApplicationContext();
    }

    @Singleton
    @Provides
    public Context providesApplicationContext() {
        return mContext;
    }

}
