package project.coppermobile.com.common.di.component;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import project.coppermobile.com.common.di.module.GatewayModule;
import project.coppermobile.com.common.di.module.LoginApplicationModule;
import project.coppermobile.com.common.di.module.ModelModule;

@Singleton
@Component(modules = {LoginApplicationModule.class, ModelModule.class, GatewayModule.class})
public interface LoginApplicationComponent {

    Context applicationContext();
}
