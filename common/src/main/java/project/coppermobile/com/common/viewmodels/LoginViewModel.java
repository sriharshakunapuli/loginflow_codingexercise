package project.coppermobile.com.common.viewmodels;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.Toast;

import project.coppermobile.com.common.cache.UserProfileData;
import project.coppermobile.com.common.response.UserProfile;
import project.coppermobile.com.common.utils.BaseViewModel;
import project.coppermobile.com.common.models.LoginModel;

public class LoginViewModel extends BaseViewModel{

    private LoginModel mModel;
    private UserProfileData userProfileData;
    private UserProfile userProfileResponse;

    private ObservableField<String> email;
    private ObservableField<String> password;
    private ObservableField<String> emailError;
    private ObservableField<String> passwordError;
    private ObservableField<String> serverMessage;

    private ObservableBoolean emailErrorVisibility;
    private ObservableBoolean passwordErrorVisibility;

    private boolean emailValue = false;
    private boolean passwordValue = false;

    public LoginViewModel(LoginModel loginModel) {
        super();
        mModel = loginModel;
        userProfileData = UserProfileData.getInstance();

        emailError = new ObservableField<>("");
        passwordError = new ObservableField<>("");
        serverMessage = new ObservableField<>("");

        emailErrorVisibility = new ObservableBoolean(false);
        passwordErrorVisibility = new ObservableBoolean(false);
    }

    @Override
    public void startLoading() {
        super.startLoading();

        userProfileResponse = mModel.readUserProfile();
        userProfileData.setUserProfileData(userProfileResponse);

    }

    public void setCredentials(String emailfield, String passwordField) {

        email = new ObservableField<>(emailfield);
        password = new ObservableField<>(passwordField);
    }

    public boolean validateCredentials() {
        emailError.set("");
        passwordError.set("");

        if (userProfileResponse != null && userProfileResponse.getUserProfile() != null) {
            if (!email.get().equalsIgnoreCase(userProfileResponse.getUserProfile().getEmail())) {
                emailErrorVisibility.set(true);
                emailError.set("E-Mail ID should be valid and not Empty");
                emailValue = false;

            } else {
                emailValue = true;
            }
            if (!password.get().equalsIgnoreCase(userProfileResponse.getUserProfile().getPassword())) {
                passwordErrorVisibility.set(true);
                passwordError.set("Please enter a vaild password");
                passwordValue = false;
            } else {
                passwordValue = true;
            }


        } else {
            serverMessage.set("Internal Server Error(400). Please relaunch the App again.");
        }

        if (emailValue && passwordValue) {
            serverMessage.set("Login Successful!(200).");
        }

        return (emailValue && passwordValue);
    }

    public ObservableField<String> getEmail() {
        return email;
    }

    public ObservableField<String> getPassword() {
        return password;
    }

    public ObservableField<String> getEmailError() {
        return emailError;
    }

    public ObservableField<String> getPasswordError() {
        return passwordError;
    }

    public ObservableField<String> getServerMessage() {
        return serverMessage;
    }

    public ObservableBoolean getEmailErrorVisibility() {
        return emailErrorVisibility;
    }

    public ObservableBoolean getPasswordErrorVisibility() {
        return passwordErrorVisibility;
    }
}
