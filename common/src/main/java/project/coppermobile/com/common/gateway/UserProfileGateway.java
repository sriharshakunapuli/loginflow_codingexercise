package project.coppermobile.com.common.gateway;

import project.coppermobile.com.common.response.UserProfile;

public interface UserProfileGateway {

    UserProfile readEmbeddedProfile();

    UserProfile readSavedProfile();

    void clearSavedProfile();
}
