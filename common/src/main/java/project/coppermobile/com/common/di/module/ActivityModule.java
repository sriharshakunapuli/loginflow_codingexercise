package project.coppermobile.com.common.di.module;

import android.app.Activity;

import java.lang.ref.WeakReference;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import project.coppermobile.com.common.DomainApplication;

@Module
public class ActivityModule {

    private WeakReference<Activity> mActivity;
    private WeakReference<DomainApplication> mDomainApplication;


    public ActivityModule(Activity activity){
        mActivity = new WeakReference<>(activity);
        if (activity == null)
            mDomainApplication = new WeakReference<>(null);
        else
            mDomainApplication = new WeakReference<>((DomainApplication)activity.getApplicationContext());
    }

    @Provides
    public Activity providesActivity(){
        return mActivity.get();
    }

    @Provides
    public DomainApplication providesDomainApplication() {
        return mDomainApplication.get();
    }

}
