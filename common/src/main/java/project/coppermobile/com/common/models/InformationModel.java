package project.coppermobile.com.common.models;

import project.coppermobile.com.common.gateway.UserProfileGateway;

public class InformationModel {

    private UserProfileGateway userProfileGateway;

    public InformationModel(UserProfileGateway userProfileGateway) {
        this.userProfileGateway = userProfileGateway;
    }

    public void clearSavedUserProfileFile() {
        userProfileGateway.clearSavedProfile();
    }
}
