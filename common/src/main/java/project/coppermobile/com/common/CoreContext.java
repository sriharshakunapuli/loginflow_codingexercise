package project.coppermobile.com.common;

import android.content.Context;
import android.content.ContextWrapper;

public class CoreContext extends ContextWrapper{

    private static final Object LOCK = new Object();
    private static CoreContext mCoreContext;

    private CoreContext(Context base) {
        super(base.getApplicationContext());
    }

    public final static CoreContext newInstance(Context context) {
        if (mCoreContext == null) {
            synchronized (LOCK) {
                if (mCoreContext == null) {
                    mCoreContext = new CoreContext(context.getApplicationContext());
                }
            }
        }

        return mCoreContext;
    }

    public static final CoreContext getContext() {
        return mCoreContext;
    }
}
