package project.coppermobile.com.common;

import android.app.Application;
import android.content.Context;

import project.coppermobile.com.common.di.component.DaggerLoginApplicationComponent;
import project.coppermobile.com.common.di.component.LoginApplicationComponent;
import project.coppermobile.com.common.di.module.LoginApplicationModule;


public class LoginApplication extends Application implements DomainApplication {

    private Application application;
    LoginApplicationComponent mComponent;
    private static LoginApplication mInstance;
    private  Context appcontext;

    @Override
    public void onCreate() {
        super.onCreate();

        setApplication(this);
        init();
    }


    public LoginApplication() {
        mInstance = this;
    }


    public static LoginApplication getInstance() {
        return LoginApplication.mInstance;
    }

    public void setApplication(Application application){
        this.application = application;
    }

    public Application getApplication() {
        return application;
    }

    public void init() {
        LoginApplication.mInstance = this;
        setAppContext(this);
        initializeDaggerComponent(getApplication());
        CoreContext.newInstance(getApplication());
    }

    public void initializeDaggerComponent(Context context) {

        mComponent = DaggerLoginApplicationComponent.builder()
                .loginApplicationModule(new LoginApplicationModule(context)).build();
    }

    private void setAppContext(Context context) {
        appcontext = context;
    }

    @Override
    public Context getContext() {
        return appcontext;
    }

    @Override
    public LoginApplicationComponent getComponent() {
        return mComponent;
    }
}
