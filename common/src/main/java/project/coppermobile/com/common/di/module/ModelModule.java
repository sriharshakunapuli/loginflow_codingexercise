package project.coppermobile.com.common.di.module;

import dagger.Module;
import dagger.Provides;
import project.coppermobile.com.common.gateway.UserProfileGateway;
import project.coppermobile.com.common.models.InformationModel;
import project.coppermobile.com.common.models.LoginModel;

@Module()
public class ModelModule {

    @Provides
    LoginModel providesLoginModel(UserProfileGateway userProfileGateway) {
        return new LoginModel(userProfileGateway);
    }

    @Provides
    InformationModel providesInformationModel(UserProfileGateway userProfileGateway) {
        return new InformationModel(userProfileGateway);
    }
}
