package project.coppermobile.com.common.di.module;

import dagger.Module;
import dagger.Provides;
import project.coppermobile.com.common.gateway.UserProfileGateway;
import project.coppermobile.com.common.providers.UserProfileGatewayProvider;

@Module
public class GatewayModule {

    @Provides
    UserProfileGateway providesUserProfileGateway() {
        return UserProfileGatewayProvider.getConfigGateway();
    }
}
