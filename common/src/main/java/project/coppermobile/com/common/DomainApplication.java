package project.coppermobile.com.common;

import android.content.Context;

import project.coppermobile.com.common.di.component.LoginApplicationComponent;

public interface DomainApplication {


    Context getContext();

    LoginApplicationComponent getComponent();
}
