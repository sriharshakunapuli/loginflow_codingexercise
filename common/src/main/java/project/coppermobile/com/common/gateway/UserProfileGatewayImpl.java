package project.coppermobile.com.common.gateway;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import project.coppermobile.com.common.CoreContext;
import project.coppermobile.com.common.LoginApplication;
import project.coppermobile.com.common.response.UserProfile;

public class UserProfileGatewayImpl implements UserProfileGateway{

    private File mProfilefDir;

    private String TAG = UserProfileGatewayImpl.class.getSimpleName();

    @Override
    public UserProfile readEmbeddedProfile() {
        UserProfile userProfileResponse;
        userProfileResponse = readSavedProfile();


        if(userProfileResponse == null) {
            AssetManager assetManager = CoreContext.getContext().getAssets();
            String filePath = "user_profile_info/UserProfileInfo.json";
            try {

                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
                InputStreamReader reader = new InputStreamReader(assetManager.open(filePath));
                UserProfile localResponse = gson.fromJson(reader, UserProfile.class);
                saveUserProfile(gson.toJson(localResponse));
                reader.close();

                if (localResponse != null) {
                    Log.i(TAG, "Local UserProfile file parsed successfully!");
                }

                return localResponse;
            } catch (IOException e) {
                Log.i(TAG, "readEmbeddedProfile error: " + e.getMessage());
            }

            //Could not read local configuration file.
            userProfileResponse = new UserProfile();
        } else {
            Log.i(TAG, "Reading saved configuration file");
        }

        return userProfileResponse;
    }

    @Override
    public UserProfile readSavedProfile(){

        Log.i(TAG, "Reading saved UserProfile file");

        File userFile = getSavedUserProfileFile();

        String jsonObj = readFile(userFile.getAbsolutePath());
        Gson gson = new GsonBuilder().create();

        UserProfile response = null;
        try {
            response = gson.fromJson(jsonObj, UserProfile.class);
        } catch (Exception e) {
            Log.i(TAG, "readSavedProfile error: " + e.getMessage());
        }
        return response;
    }

    private void saveUserProfile(String jsonResponse) throws IOException {

        File userDir = getSavedUserProfileDirectory();
        if (!userDir.exists()) {
            Log.i(TAG, "UserProfile directory did not exist and was created!");
            if (!userDir.mkdirs()) {
                Log.i(TAG, "Failed creating directory " + userDir.getAbsolutePath());
            }
        }

        File userFile = getSavedUserProfileFile();

        clearSavedProfile();

        Log.i(TAG, "Saving configurations!!");
        try {
            FileOutputStream outputStream = new FileOutputStream(userFile);
            outputStream.write(jsonResponse.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*BufferedWriter confWriter = new BufferedWriter(new FileWriter(userFile, true));
        confWriter.write(jsonResponse);
        confWriter.close();*/
    }

    private String readFile(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        }
        catch (IOException e) {
            Log.i(TAG, "readFile error: No saved configuration found!");
        }
        return contentBuilder.toString();
    }

    @Override
    public void clearSavedProfile() {
        try {
            PrintWriter writer = new PrintWriter(getSavedUserProfileFile());
            writer.print("");
            writer.close();
        } catch (IOException e) {
            Log.i(TAG, "clearSavedProfile error: " + e.getMessage());
        }
    }

    private File getSavedUserProfileFile(){
        if(mProfilefDir == null) {
            mProfilefDir = new File(getSavedUserProfileDirectory(), "user_profile_info/UserProfileInfo.json");
        }
        return mProfilefDir;
    }

    private File getSavedUserProfileDirectory(){
        if(mProfilefDir == null){
            mProfilefDir = new File(CoreContext.getContext().getCacheDir(), "user");
        }
        return mProfilefDir;
    }

    public UserProfile parseJsonConfiguration(String jsonResponse){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
        return gson.fromJson(jsonResponse, UserProfile.class);
    }
}
