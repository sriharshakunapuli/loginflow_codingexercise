package project.coppermobile.com.common.di.module;

import dagger.Module;
import dagger.Provides;
import project.coppermobile.com.common.models.InformationModel;
import project.coppermobile.com.common.models.LoginModel;
import project.coppermobile.com.common.viewmodels.InformationViewModel;
import project.coppermobile.com.common.viewmodels.LoginViewModel;

@Module(includes = {ModelModule.class, GatewayModule.class})
public class VieModelModule {

    @Provides
    LoginViewModel providesLoginViewModel(LoginModel loginModel) {
        return new LoginViewModel(loginModel);
    }

    @Provides
    InformationViewModel providesInformationViewModel(InformationModel informationModel) {
        return new InformationViewModel(informationModel);
    }
}
