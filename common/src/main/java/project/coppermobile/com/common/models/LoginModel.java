package project.coppermobile.com.common.models;

import project.coppermobile.com.common.gateway.UserProfileGateway;
import project.coppermobile.com.common.response.UserProfile;

public class LoginModel {

    private UserProfileGateway userProfileGateway;

    public LoginModel(UserProfileGateway userProfileGateway) {
        this.userProfileGateway = userProfileGateway;
    }

    public UserProfile readUserProfile() {
        return userProfileGateway.readEmbeddedProfile();
    }
}
