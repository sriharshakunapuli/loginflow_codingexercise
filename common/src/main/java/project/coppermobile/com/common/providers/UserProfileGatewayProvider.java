package project.coppermobile.com.common.providers;

import project.coppermobile.com.common.gateway.UserProfileGateway;
import project.coppermobile.com.common.gateway.UserProfileGatewayImpl;

public class UserProfileGatewayProvider {

    public static UserProfileGateway getConfigGateway() {
        return new UserProfileGatewayImpl();
    }
}
