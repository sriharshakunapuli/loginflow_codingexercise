package project.coppermobile.com.common.utils;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;

public abstract class BaseActivity<VM extends BaseViewModel> extends AppCompatActivity {

    private VM mViewModel;
    private SparseArray<BaseViewModel> mBaseViewModelSparseArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeComponent();
        mViewModel = onCreateViewModel();
        /*mBaseViewModelSparseArray = onCreateViewModels();
        if(mBaseViewModelSparseArray == null) {
            mBaseViewModelSparseArray = new SparseArray<>();
            mBaseViewModelSparseArray.put(0, mViewModel);
        }
        for (int index = 0; index < mBaseViewModelSparseArray.size(); index++) {
            BaseViewModel baseViewModel = mBaseViewModelSparseArray.get(index);
            baseViewModel.onCreate(savedInstanceState, getIntent().getExtras());
        }*/

        if (mViewModel != null) {
            mViewModel.startLoading();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        /*for (int index = 0; index < mBaseViewModelSparseArray.size(); index++) {
            mBaseViewModelSparseArray.get(index).onStart();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        /*for (int index = 0; index < mBaseViewModelSparseArray.size(); index++) {
            mBaseViewModelSparseArray.get(index).onStop();
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*for (int index = 0; index < mBaseViewModelSparseArray.size(); index++) {
            mBaseViewModelSparseArray.get(index).onDestroy();
        }*/
    }

    public abstract VM onCreateViewModel();

    public abstract void initializeComponent();
}
