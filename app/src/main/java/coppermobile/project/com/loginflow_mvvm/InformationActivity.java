package coppermobile.project.com.loginflow_mvvm;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.StrictMode;

import javax.inject.Inject;

import coppermobile.project.com.loginflow_mvvm.databinding.ActivityInformationBinding;
import coppermobile.project.com.loginflow_mvvm.di.ActivityBasicComponent;
import coppermobile.project.com.loginflow_mvvm.di.DaggerActivityBasicComponent;
import project.coppermobile.com.common.utils.BaseActivity;
import project.coppermobile.com.common.DomainApplication;
import project.coppermobile.com.common.di.module.ActivityModule;
import project.coppermobile.com.common.viewmodels.InformationViewModel;

public class InformationActivity extends BaseActivity<InformationViewModel> {

    @Inject
    InformationViewModel informationViewModel;

    private ActivityInformationBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_information);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_information);
        mBinding.setViewModel(informationViewModel);
    }

    @Override
    public InformationViewModel onCreateViewModel() {
        return informationViewModel;
    }

    @Override
    public void initializeComponent() {
        ActivityBasicComponent activityBasicComponent = DaggerActivityBasicComponent.builder()
                .loginApplicationComponent(((DomainApplication) getApplicationContext()).getComponent())
                .activityModule(new ActivityModule(this))
                .build();
        activityBasicComponent.inject(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        informationViewModel.displayInfo();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        informationViewModel.clearSavedInfo();
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
}
