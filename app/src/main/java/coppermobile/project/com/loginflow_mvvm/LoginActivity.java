package coppermobile.project.com.loginflow_mvvm;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import coppermobile.project.com.loginflow_mvvm.databinding.ActivityLoginBinding;
import coppermobile.project.com.loginflow_mvvm.di.ActivityBasicComponent;
import coppermobile.project.com.loginflow_mvvm.di.DaggerActivityBasicComponent;
import project.coppermobile.com.common.utils.BaseActivity;
import project.coppermobile.com.common.DomainApplication;
import project.coppermobile.com.common.di.module.ActivityModule;
import project.coppermobile.com.common.viewmodels.LoginViewModel;

public class LoginActivity extends BaseActivity<LoginViewModel> {

    @Inject
    LoginViewModel loginViewModel;

    private ActivityLoginBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mBinding.setViewModel(loginViewModel);

        mBinding.signinbtn.setOnClickListener(mClickListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        loginViewModel.startLoading();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public LoginViewModel onCreateViewModel() {
        return loginViewModel;
    }

    @Override
    public void initializeComponent() {
        ActivityBasicComponent activityBasicComponent = DaggerActivityBasicComponent.builder()
                .loginApplicationComponent(((DomainApplication) getApplicationContext()).getComponent())
                .activityModule(new ActivityModule(this))
                .build();
        activityBasicComponent.inject(this);
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            loginViewModel.setCredentials(mBinding.emailfield.getText().toString(),
                    mBinding.passwordfield.getText().toString());

            if (loginViewModel.validateCredentials()) startIntent();

        }
    };

    private void startIntent() {
        Intent intent = new Intent(this, InformationActivity.class);
        startActivity(intent);
    }
}
