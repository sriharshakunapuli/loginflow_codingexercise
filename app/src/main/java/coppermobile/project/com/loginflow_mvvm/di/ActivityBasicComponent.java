package coppermobile.project.com.loginflow_mvvm.di;

import coppermobile.project.com.loginflow_mvvm.InformationActivity;
import coppermobile.project.com.loginflow_mvvm.LoginActivity;
import dagger.Component;
import project.coppermobile.com.common.utils.PerActivity;
import project.coppermobile.com.common.di.component.LoginApplicationComponent;
import project.coppermobile.com.common.di.module.ActivityModule;
import project.coppermobile.com.common.di.module.VieModelModule;



@PerActivity
@Component(dependencies = {LoginApplicationComponent.class}, modules = {VieModelModule.class, ActivityModule.class})
public interface ActivityBasicComponent {

    void inject(LoginActivity loginActivity);

    void inject(InformationActivity informationActivity);
}
